# mvplan

MVPlan Java Dive Planner

This soft could be build with maven (on linux: `apt-get install maven`)

Source code is available here: https://framagit.org/mvplan-dev/mvplan

## IDE used

IDE used was netbeans (signs in forms files, "org.netbeans.modules.form.forminfo.JPanelFormInfo") ... but that's not our favorite IDE so it could change in some times :-)


## Translations

Translations can be done online here thanks to framasoft zanata service: https://trad.framasoft.org/project/view/mvplan?dswid=-449

Note:

* install zanata cli: http://docs.zanata.org/en/release/client/installation/linux-installation/
* push translations from git to zanata : `zanata-cli push --push-type trans --src-lang en  --locales fr,it,pl,pt`
* pull new translations from zanata to git : `zanata-cli pull`

